# -*- coding: utf-8 -*-

from PIL import Image


class FileService(object):

    def get_image_size(self, data):
        """
        This method return image size which is saved in
        specified path.
        """
        with Image.open(data) as image:
            try:
                return image.size
            except IOError:
                print("Could not read specified data")

    def save_image_file(self, data, img_id):
        file_name = "fox_image{}.jpg".format(img_id)
        with open(file_name, 'wb') as file:
            file.write(data)
        return file_name



