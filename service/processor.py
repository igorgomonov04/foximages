# -*- coding: utf-8 -*-


class ServiceProcessor(object):

    def __init__(self, network_service, db_service, file_service):
        self._network_service = network_service
        self._file_service = file_service
        self._db_service = db_service

    def get_resource(self):
        return self._network_service.get_resource()

    def get_image_from_url(self, img_url):
        return self._network_service.get_image_from_url(img_url=img_url)

    def get_image_url(self, data):
        return self._network_service.get_image_url(data=data)

    def get_image_id(self, img_url):
        return self._network_service.get_image_id(img_url=img_url)

    def save_image_file(self, data, img_id):
        return self._file_service.save_image_file(data=data, img_id=img_id)

    def get_image_size(self, data):
        return self._file_service.get_image_size(data=data)

    def save_image_data_to_database(self, img_id, img_size, img_address):
        return self._db_service.save_image_data(
            img_id=img_id, img_size=img_size, img_address=img_address
        )

    def get_image_from_db_by_id(self, img_id):
        return self._db_service.get_image_from_db_by_id(img_id=img_id)

    def get_image_from_db_by_address(self, img_address):
        return self._db_service.get_image_from_db_by_address(
            img_address=img_address
        )
