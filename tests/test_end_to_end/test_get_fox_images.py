# -*- coding: utf-8 -*-

from service.database_service.database import Database
from service.file_service.file import FileService
from service.network_service.network import RealNetwork
from service.processor import ServiceProcessor

from tests.verify_steps.database import(
    verify_image_id_in_db, verify_image_address_in_db, verify_image_size_in_db
)

service = ServiceProcessor(
    network_service=RealNetwork(),
    db_service=Database(),
    file_service=FileService()
)


def test_image_id_in_db():
    data = service.get_resource()
    img_url = service.get_image_url(data=data)
    img_id = service.get_image_id(img_url=img_url)
    image = service.get_image_from_url(img_url=img_url)
    file = service.save_image_file(data=image, img_id=img_id)
    img_size = service.get_image_size(data=file)
    service.save_image_data_to_database(
        img_id=img_id, img_size=img_size, img_address=img_url
    )
    verify_image_id_in_db(service=service, img_address=img_url, img_id=img_id)


def test_img_address_in_db():
    data = service.get_resource()
    img_url = service.get_image_url(data=data)
    img_id = service.get_image_id(img_url=img_url)
    image = service.get_image_from_url(img_url=img_url)
    file = service.save_image_file(data=image, img_id=img_id)
    img_size = service.get_image_size(data=file)
    service.save_image_data_to_database(
        img_id=img_id, img_size=img_size, img_address=img_url
    )
    verify_image_address_in_db(
        service=service, img_id=img_id, img_address=img_url
    )


def test_img_size_id_db():
    data = service.get_resource()
    img_url = service.get_image_url(data=data)
    img_id = service.get_image_id(img_url=img_url)
    image = service.get_image_from_url(img_url=img_url)
    file = service.save_image_file(data=image, img_id=img_id)
    img_size = service.get_image_size(data=file)
    service.save_image_data_to_database(
        img_id=img_id, img_size=img_size, img_address=img_url
    )
    verify_image_size_in_db(service=service, img_id=img_id, img_size=img_size)
