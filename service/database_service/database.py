# -*- coding: utf-8 -*-

import os
import sqlite3
import time


# create a default path to connect to and create (if necessary) a database
DEFAULT_PATH = os.path.join(
    os.path.dirname(__file__), 'fox_database.sqlite3'
)


class Database(object):

    def __init__(self):
        self.connection = sqlite3.connect(DEFAULT_PATH)
        query = """
                create table images (
                    id integer serial primary key,
                    address text,
                    saved_time timestamp,
                    image_width integer,
                    image_height integer
                )"""
        try:
            self.connection.execute(query)
        except sqlite3.OperationalError:
            return

    def save_image_data(self, img_id, img_address, img_size):
        """
        This method insert specified image info to store it in database.
        :param img_id: unique image_id from url
        :param img_address: image address url
        :param img_size: image size
        """
        query = """
        insert into images (
            id, address, saved_time, image_width, image_height
            ) values(?,?,?,?,?)
        """
        saved_time = time.time()
        try:
            self.connection.execute(
                query, (
                    img_id, img_address, saved_time, img_size[0], img_size[1]
                )
            )
            self.connection.commit()
        except sqlite3.OperationalError as error:
            print("Something goes wrong with database")
            print(str(error))
        except sqlite3.IntegrityError:
            print("Seems like we already image with such id in database")

    def get_image_from_db_by_id(self, img_id):
        """
        This method will return image data,
        stored in database if it exist there using img_id.
        """
        query = """
        select * from images where id = ?
        """
        img_data = None
        try:
            img_data = self.connection.execute(query, [img_id])
            self.connection.commit()
        except sqlite3.OperationalError as error:
            print("Something goes wrong with database")
            print(str(error))
        return img_data

    def get_image_from_db_by_address(self, img_address):
        """
        This method will return image data,
        stored in database id it exist there, using img_address.
        """
        query = """
        select * from images where address = ?
        """
        img_data = None
        try:
            img_data = self.connection.execute(query, [img_address])
            self.connection.commit()
        except sqlite3.OperationalError as error:
            print("Something goes wrong with database")
            print(str(error))
        return img_data
