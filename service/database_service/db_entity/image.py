# -*- coding: utf-8 -*-

from service.database_service.db_entity.keys import ImageDBKeys
from utils.helpers import generate, generate_time


def image_entity(img_id=None, img_address=None, saved_time=None,
                 img_size=None):
    keys = ImageDBKeys
    body = {
        img_id: {
            keys.Address: img_address,
            keys.SavedTime: generate(saved_time, generate_time()),
            keys.ImageWidth: img_size[0],
            keys.ImageHeight: img_size[1],
        }
    }
    return body
