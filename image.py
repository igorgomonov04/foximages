# -*- coding: utf-8 -*-


class FoxImages(object):

    @staticmethod
    def get_fox_images(service):
        data = service.get_resource()
        img_url = service.get_image_url(data=data)
        img_id = service.get_image_id(img_url=img_url)
        image = service.get_image_from_url(img_url=img_url)
        file = service.save_image_file(data=image, img_id=img_id)
        img_size = service.get_image_size(data=file)
        service.save_image_data_to_database(
            img_id=img_id, img_size=img_size, img_address=img_url
        )
        print("Operation complete, fox image successfully downloaded")
