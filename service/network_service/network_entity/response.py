# -*- coding: utf-8 -*-

from service.network_service.network_entity.keys import NetworkResponseKeys
from utils.helpers import generate, generate_image_link, generate_url


def random_fox_response(image=None, link=None):
    body = {
        NetworkResponseKeys.Image: generate(image, generate_image_link()),
        NetworkResponseKeys.Link: generate(link, generate_url()),
    }
    return body
