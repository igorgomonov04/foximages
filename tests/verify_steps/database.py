# -*- coding: utf-8 -*-


def verify_image_id_in_db(service, img_address, img_id):
    db_resp = service.get_image_from_db_by_address(
        img_address=img_address
    )
    db_data = db_resp.fetchall()
    db_img_id = db_data[0][0]
    assert(db_img_id == img_id)


def verify_image_address_in_db(service, img_id, img_address):
    db_resp = service.get_image_from_db_by_id(img_id=img_id)
    db_data = db_resp.fetchall()
    db_img_address = db_data[0][1]
    assert(db_img_address == img_address)


def verify_image_size_in_db(service, img_id, img_size):
    db_resp = service.get_image_from_db_by_id(img_id=img_id)
    db_data = db_resp.fetchall()
    db_img_size = (db_data[0][3], db_data[0][4])
    assert (db_img_size == img_size)
