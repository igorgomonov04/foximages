# -*- coding: utf-8 -*-

import random

from service.database_service.database import Database
from service.file_service.file import FileService
from service.network_service.network import TestNetwork
from service.network_service.network_entity.response import random_fox_response
from service.processor import ServiceProcessor

from tests.verify_steps import network
from utils.helpers import generate_string, generate_image_link


service = ServiceProcessor(
    network_service=TestNetwork(),
    db_service=Database(),
    file_service=FileService()
)


def test_get_resource():
    response = random_fox_response()
    network.verify_data(service=service, response=response)


def test_get_img_url():
    img_url = generate_string()
    response = random_fox_response(image=img_url)
    network.verify_get_image_url(
        service=service, response=response, img_url=img_url
    )


def test_get_img_id():
    img_id = random.randint(1, 100)
    img_url = generate_image_link(img_id=img_id)
    network.verify_get_image_id(
        service=service, img_url=img_url, img_id=img_id
    )


def test_get_img_url_negative():
    response = {
        generate_string(): generate_string()
    }
    network.verify_get_image_url_negative(service=service, response=response)


def test_get_img_id_negative():
    img_url = generate_string()
    network.verify_get_image_id_negative(service=service, img_url=img_url)
