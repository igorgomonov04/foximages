# -*- coding: utf-8 -*-

import pytest

from service.network_service.network import TestNetwork


def verify_data(service, response):
    TestNetwork.data = response
    assert (service.get_resource() == response)


def verify_get_image_url(service, response, img_url):
    assert (service.get_image_url(data=response) == img_url)


def verify_get_image_url_negative(service, response):
    with pytest.raises(KeyError):
        service.get_image_url(data=response)


def verify_get_image_id(service, img_url, img_id):
    assert (service.get_image_id(img_url) == img_id)


def verify_get_image_id_negative(service, img_url):
    with pytest.raises(AttributeError):
        service.get_image_id(img_url=img_url)
