# -*- coding: utf-8 -*-

from image import FoxImages
from service.database_service.database import Database
from service.file_service.file import FileService
from service.network_service.network import RealNetwork
from service.processor import ServiceProcessor

if __name__ == '__main__':
    service = ServiceProcessor(
        network_service=RealNetwork(),
        db_service=Database(),
        file_service=FileService()
    )
    FoxImages.get_fox_images(service)
