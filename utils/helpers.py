# -*- coding: utf-8 -*-

import random
import string
import time


def generate_string(size=None):
    chars = string.ascii_uppercase
    return ''.join(random.choice(chars) for _ in range(
        size or random.randint(2, 10)
    ))


def generate_size():
    return random.randint(10, 1000), random.randint(10, 1000)


def generate_id():
    return random.randint(2, 1000)


def generate_time():
    return time.time()


def generate(param_one, param_two):
    if param_one:
        return param_one
    return param_two


def generate_image_link(img_id=None):
    return "http://randomfox.ca/images/{}.jpg".format(
        generate(img_id, random.randint(1, 1000))
    )


def generate_url(img_id=None):
    return "http://randomfox.ca/images/{}".format(
        generate(img_id, random.randint(1, 1000))
    )
