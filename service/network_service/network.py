# -*- coding: utf-8 -*-

import re

import requests

from service.network_service.network_entity.keys import NetworkResponseKeys


class Network(object):

    def get_resource(self):
        raise NotImplementedError

    def get_image_from_url(self, img_url):
        raise NotImplementedError

    def get_image_url(self, data):
        raise NotImplementedError

    def get_image_id(self, img_url):
        raise NotImplementedError


class RealNetwork(Network):

    RESOURCE_URL = "https://randomfox.ca/floof/"

    def get_resource(self):
        """
        This method get a call to external http resource with specified url
        in constants.
        """
        response = None
        try:
            response = requests.get(url=RealNetwork.RESOURCE_URL)
        except requests.HTTPError as error:
            print("Oops, we got an http error: {}".format(str(error)))
        except requests.ConnectionError as error:
            print("Huston, we have a problem: {}".format(str(error)))
        except requests.Timeout as error:
            print("Oops, we got ad timeout error: {}".format(str(error)))
        except requests.RequestException as error:
            print("Oops, we got an request error: {}".format(str(error)))
        finally:
            return response.json()

    def get_image_from_url(self, img_url):
        """
        This method will return downloaded image.
        """
        response = None
        try:
            response = requests.get(url=img_url)
        except requests.HTTPError as error:
            print("Oops, we got an http error: {}".format(str(error)))
        except requests.ConnectionError as error:
            print("Huston, we have a problem: {}".format(str(error)))
        except requests.Timeout as error:
            print("Oops, we got ad timeout error: {}".format(str(error)))
        except requests.RequestException as error:
            print("Oops, we got an request error: {}".format(str(error)))
        finally:
            return response.content

    def get_image_url(self, data):
        """
        This method get json data and return image url needs to be download.
        """
        return data[NetworkResponseKeys.Image]

    def get_image_id(self, img_url):
        """
        This method will return image id which is get from the response.
        """
        return int(re.search(r'\d+', img_url).group())


class TestNetwork(Network):
    img_url = None
    image = None
    image_id = None
    data = {}

    def get_resource(self):
        """
        This is a mock realization of http call.
        """
        return self.data

    def get_image_from_url(self, img_url):
        """
        This is a mock realization of http call, return bytes.
        """
        return self.image

    def get_image_url(self, data):
        """
        This method is a mock realization of get image url method which is
        return random string.
        """
        return data[NetworkResponseKeys.Image]

    def get_image_id(self, img_url):
        """
        This method will return image id which is get from the response.
        """
        return int(re.search(r'\d+', img_url).group())
