# -*- coding: utf-8 -*-


class ImageDBKeys(object):
    Address = u'address'
    SavedTime = u'saved_time'
    ImageWidth = u'image_width'
    ImageHeight = u'image_height'

